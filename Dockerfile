FROM debian
RUN apt update 
RUN apt install apache2 libapache2-mod-php php php-mysql -y
COPY web /var/www/html/
ENV MYSQL_HOST=172.17.0.2
ENV MYSQL_USERNAME=root
ENV MYSQL_PASSWORD=123
ENV MYSQL_DB=sistemas
EXPOSE 80
CMD ["/bin/bash","-c","set -e;apachectl -D FOREGROUND \"$@\" "]
